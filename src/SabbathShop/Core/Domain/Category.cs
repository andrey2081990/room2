﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain
{
    public class Category: BaseEntity
    {
        [MaxLength(256), Required]
        public string Title { get; set; }


        /**
         * Relations
         */
        public Guid ProductId { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
