﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain
{
    public class Sale: BaseEntity
    {

        [Required]
        public string Quantity { get; set; }

        [Required]
        public int Percentage { get; set; }

        public DateTime DateStart { get; set; }

        public DateTime DateEnd { get; set; }

        /**
         * Relations
         */

        //public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
