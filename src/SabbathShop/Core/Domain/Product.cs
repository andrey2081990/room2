﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain
{
    public class Product: BaseEntity
    {
        [MaxLength(256), Required]
        public string Title { get; set; }

        [Required]
        public int Sort{ get; set; }

        public string Description { get; set; }

        [Required]
        public decimal Price { get; set; }

        public string MediaUrl { get; set; }


        /**
         * Relations
         */
        public Guid CategoryId { get; set; }
        public virtual ICollection<Category> Categorys { get; set; }

    }
}
