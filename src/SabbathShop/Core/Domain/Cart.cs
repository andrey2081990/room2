﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain
{
    public class Cart: BaseEntity
    {

        [Required]
        public string Quantity { get; set; }

        /**
         * Relations
         */

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
