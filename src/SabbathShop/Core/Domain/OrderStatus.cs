﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain
{
    public class OrderStatus: BaseEntity
    {
        [MaxLength(256), Required]
        public string Title { get; set; }

        /**
         * Relations
         */
        public virtual ICollection<Order>Orders { get; set; }

    }
}
