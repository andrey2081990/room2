﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain
{
    public class User: BaseEntity
    {
        [MaxLength(256), Required]
        public string FirstName { get; set; }

        [MaxLength(256)]
        public string LastName { get; set; }

        public virtual string FullName => $"{FirstName} {LastName}";

        [MaxLength(256), Required]
        public string Email { get; set; }

        [MaxLength(32), Required]
        public string Phone { get; set; }

        /**
         * Relations
         */
        public Guid RoleId { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
