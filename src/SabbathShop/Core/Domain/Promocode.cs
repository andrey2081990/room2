﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain
{
    public class Promocode: BaseEntity
    {
        [Required]
        public bool IsActive{ get; set; }

        [MaxLength(256), Required]
        public string Title { get; set; }

        [Required]
        public decimal Nominal { get; set; }

        [Required]
        public bool IsPercentage { get; set; }

        /**
         * Relations
         */
        public virtual ICollection<Order> Orders { get; set; }

    }
}
