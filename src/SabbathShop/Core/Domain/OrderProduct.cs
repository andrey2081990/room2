﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain
{
    public class OrderProduct: BaseEntity
    {

        [Required]
        public string Quantity { get; set; }

        [Required]
        public decimal Price { get; set; }


        /**
         * Relations
         */
        
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }

        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
