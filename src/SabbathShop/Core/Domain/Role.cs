﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain
{
    public enum RoleType
    {
        Admin,
        User,
        Manager
    }

    public class Role : BaseEntity
    {
        [MaxLength(50), Required]
        public string Title { get; set; }

        public RoleType Type { get; set; }

        //public virtual ICollection<User> Users { get; set; }

    }
}