﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain
{
    public class Order: BaseEntity
    {
        [MaxLength(256), Required]
        public string Number { get; set; }

        [Required, MaxLength(32)]
        public string Phone { get; set; }

        [Required]
        public int Address { get; set; }

        public int Description { get; set; }


        /**
         * Relations
         */
        
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int OrderStatusId { get; set; }
        public virtual OrderStatus Status { get; set; }

        public int PromocodeId { get; set; }
        public virtual Promocode Promocode { get; set; }
    }
}
