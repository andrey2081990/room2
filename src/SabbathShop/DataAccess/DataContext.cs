﻿using Microsoft.EntityFrameworkCore;
using Core.Domain;

namespace DataAccess
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) {}

        public DbSet<Product> Product { get; set; }
        public DbSet<Category> Category { get; set; }

        public DbSet<Cart> Cart { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderProduct> OrderProduct { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }

        public DbSet<Promocode> Promocode { get; set; }
        public DbSet<Sale> Sale { get; set; }

        public DbSet<User> User { get; set; }
        public DbSet<Role> Role { get; set; }
    }
}