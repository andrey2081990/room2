using API.Infrastructure;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using API.Middlewares;
using NLog;
using NLog.Web;
using Microsoft.AspNetCore.Identity;

// Early init of NLog to allow startup and exception logging, before host is built
var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("init main");

try
{
    var builder = WebApplication.CreateBuilder(args);
    
    // NLog: Setup NLog for Dependency injection
    builder.Logging.ClearProviders();
    builder.Host.UseNLog();

    // Add services to the container.
    builder.Services.AddControllers();

    builder.Services.AddDbContext<DataContext>(options =>
    {
        //options.UseSqlite(builder.Configuration.GetConnectionString("Sqlite"));
        options.UseSqlServer(builder.Configuration.GetConnectionString("SqlServer"));
    });

    builder.Services.AddAutoMapper(typeof(MappingProfiles).Assembly);

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen(c =>
    {
        c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory,
            $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
    });
    builder.Services.AddCors();

    var app = builder.Build();
    
    
    app.UseCors(builder =>
    {
        builder.WithOrigins("http://localhost:5173");
        builder.AllowAnyHeader();
        builder.AllowAnyMethod();
    });

    using var scope = app.Services.CreateScope();
    var services = scope.ServiceProvider;

    var context = services.GetRequiredService<DataContext>();
    //var userManager = services.GetRequiredService<UserManager<AppUser>>();
    context.Database.EnsureDeleted();
    context.Database.EnsureCreated();
    //context.Database.Migrate();
    Seed.SeedData(context);

// Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseAuthorization();
    app.UseMiddleware<ResponseWrapperMiddleware>();

    app.MapControllers();

    app.Run();
}
catch (Exception exception)
{
    // NLog: catch setup errors
    logger.Error(exception, "Stopped program because of exception");
    throw;
}
finally
{
    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
    NLog.LogManager.Shutdown();
}