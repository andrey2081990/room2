﻿using API.Models.Cart;
using API.Models.Category;
using API.Models.Order;
using API.Models.OrderStatus;
using API.Models.Product;
using API.Models.Promocode;
using API.Models.Role;
using API.Models.Sale;
using API.Models.User;
using AutoMapper;
using Core.Domain;
using System.Diagnostics;

namespace API.Infrastructure
{
    public class MappingProfiles : AutoMapper.Profile
    {
        public MappingProfiles()
        {
            CreateMap<Product, ProductResponse>()
                .ForMember(p => p.Category, opt => opt.MapFrom(src => src.Categorys.FirstOrDefault().Title));
            CreateMap<ProductEdit, Product>();
            CreateMap<ProductNew, Product>()
                .ForMember(p => p.Id, n => n.MapFrom(id => Guid.NewGuid()));

            CreateMap<User, UserResponse>()
               .ForMember(p => p.Role, opt => opt.MapFrom(src => src.Roles.FirstOrDefault().Title));
            CreateMap<UserEdit, User>();
            CreateMap<UserNew, User>()
                .ForMember(p => p.Id, n => n.MapFrom(id => Guid.NewGuid()));

            CreateMap<Role, RoleResponse>();
            CreateMap<RoleEdit, Role>();
            CreateMap<RoleNew, Role>()
                .ForMember(p => p.Id, n => n.MapFrom(id => Guid.NewGuid()));

            CreateMap<Category, CategoryResponse>();
            CreateMap<CategoryEdit, Category>();
            CreateMap<CategoryNew, Category>()
                .ForMember(p => p.Id, n => n.MapFrom(id => Guid.NewGuid()));

            CreateMap<Sale, SaleResponse>()
                .ForMember(p => p.Product, opt => opt.MapFrom(src => src.Product.Title)); ;
            CreateMap<SaleEdit, Sale>();
            CreateMap<SaleNew, Sale>()
                .ForMember(p => p.Id, n => n.MapFrom(id => Guid.NewGuid()));

            CreateMap<Promocode, PromocodeResponse>();
            CreateMap<PromocodeEdit, Promocode>();
            CreateMap<PromocodeNew, Promocode>()
                .ForMember(p => p.Id, n => n.MapFrom(id => Guid.NewGuid()));

            CreateMap<OrderStatus, OrderStatusResponse>();
            CreateMap<OrderStatusEdit, OrderStatus>();
            CreateMap<OrderStatusNew, OrderStatus>()
                .ForMember(p => p.Id, n => n.MapFrom(id => Guid.NewGuid()));

            CreateMap<Cart, CartResponse>();
            CreateMap<CartEdit, Cart>();
            CreateMap<CartNew, Cart>()
                .ForMember(p => p.Id, n => n.MapFrom(id => Guid.NewGuid()));

            CreateMap<Order, OrderResponse>();
            CreateMap<OrderEdit, Order>();
            CreateMap<OrderNew, Order>()
                .ForMember(p => p.Id, n => n.MapFrom(id => Guid.NewGuid()));


        }
    }
}
