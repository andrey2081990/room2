using API.Models.Role;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.Domain;
using DataAccess;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace API.Controllers
{
    /// <summary>
    /// Продукты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public RolesController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные по всем ролям
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Role>>> GetRoles()
        {
            var roles = await _context.Role
                .ProjectTo<RoleResponse>(_mapper.ConfigurationProvider).ToListAsync();

            return Ok(roles);
        }

        /// <summary>
        /// Получить данные роли по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<RoleResponse>> GetRole(Guid id)
        {
            var role = await _context.Role.FindAsync(id);
            if (role == null) return NotFound();
            var roleResponse = _mapper.Map<RoleResponse>(role);

            return Ok(roleResponse);
        }

        /// <summary>
        /// Создать роль
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateRole(RoleNew request)
        {
            var role = _mapper.Map<Role>(request);

            _context.Role.Add(role);
            await _context.SaveChangesAsync();

            return Ok(new { id = role.Id });
        }

        /// <summary>
        /// Изменить данные роли
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> EditRole(RoleEdit request)
        {
            var role = await _context.Role.FindAsync(request.Id);
            if (role == null) return NotFound();

            role.Title = request.Title;
           
            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Удалить роль по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteRole(Guid id)
        {
            var role = await _context.Role.FindAsync(id);
            if (role == null) return NotFound();
            _context.Role.Remove(role);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}