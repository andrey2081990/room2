using API.Models.Product;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.Domain;
using DataAccess;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace API.Controllers
{
    /// <summary>
    /// Продукты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductsController> _logger;

        public ProductsController(DataContext context, IMapper mapper, ILogger<ProductsController> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Получить данные по всем продуктам
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Product>>> GetProducts()
        {
            _logger.LogInformation("Get all products");
            var products = await _context.Product
                .ProjectTo<ProductResponse>(_mapper.ConfigurationProvider).ToListAsync();

            return Ok(products);
        }

        /// <summary>
        /// Получить данные продукта по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<ProductResponse>> GetProduct(Guid id)
        {
            var product = await _context.Product.FindAsync(id);
            if (product == null) return NotFound();
            var productResponse = _mapper.Map<ProductResponse>(product);

            return Ok(productResponse);
        }

        /// <summary>
        /// Создать Продукт
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateProduct(ProductNew request)
        {
            var product = _mapper.Map<Product>(request);

            Category category = _context.Category.FirstOrDefault(x => x.Title == request.Category);
            if (category == null) return NotFound();

            product.CategoryId = category.Id;
            product.Categorys = new List<Category>();
            product.Categorys.Add(category);
            _context.Product.Add(product);
            await _context.SaveChangesAsync();

            return Ok(new { id = product.Id });
        }

        /// <summary>
        /// Изменить данные продукта
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> EditProduct(ProductEdit request)
        {
            var product = await _context.Product.FindAsync(request.Id);
            if (product == null) return NotFound();

            product.Title = request.Title;
            product.Description = request.Description;
            product.Price = request.Price;
            product.Sort = request.Sort;    

            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Удалить продукт по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteProduct(Guid id)
        {
            var product = await _context.Product.FindAsync(id);
            if (product == null) return NotFound();
            _context.Product.Remove(product);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}