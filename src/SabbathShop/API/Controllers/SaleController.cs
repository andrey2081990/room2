﻿using API.Models.Sale;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.Domain;
using DataAccess;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace API.Controllers
{
    /// <summary>
    /// Скидки
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public SaleController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные по всем скидкам
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Sale>>> GetSales()
        {
            var sales = await _context.Sale
                .ProjectTo<SaleResponse>(_mapper.ConfigurationProvider).ToListAsync();

            return Ok(sales);
        }

        /// <summary>
        /// Получить данные скидки по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<SaleResponse>> GetSale(Guid id)
        {
            var sale = await _context.Sale.FindAsync(id);
            if (sale == null) return NotFound();
            var saleResponse = _mapper.Map<SaleResponse>(sale);

            return Ok(saleResponse);
        }

        /// <summary>
        /// Создать скидку
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateSale(SaleNew request)
        {
            var sale = _mapper.Map<Sale>(request);
            var product =await _context.Product.FindAsync(request.ProductId);
            if (product == null) return NotFound();
            sale.Product = product;
            _context.Sale.Add(sale);
            await _context.SaveChangesAsync();

            return Ok(new { id = sale.Id });
        }

        /// <summary>
        /// Изменить данные о скидки
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> EditSale(SaleEdit request)
        {
            var sale = await _context.Sale.FindAsync(request.Id);
            if (sale == null) return NotFound();

            sale.Quantity = request.Quantity;
            sale.Percentage = request.Percentage;
            sale.DateStart = request.DateStart;
            sale.DateEnd = request.DateEnd;

            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Удалить скидки по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteSale(Guid id)
        {
            var sale = await _context.Sale.FindAsync(id);
            if (sale == null) return NotFound();
            _context.Sale.Remove(sale);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}
