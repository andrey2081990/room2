using API.Models.OrderStatus;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.Domain;
using DataAccess;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace API.Controllers
{
    /// <summary>
    /// Продукты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class OrderStatusController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public OrderStatusController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные по всем статусам
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<OrderStatus>>> GetOrderStatuses()
        {
            var orderStatuses = await _context.OrderStatus
                .ProjectTo<OrderStatusResponse>(_mapper.ConfigurationProvider).ToListAsync();

            return Ok(orderStatuses);
        }

        /// <summary>
        /// Получить данные статуса по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<OrderStatusResponse>> GetOrderStatus(Guid id)
        {
            var orderStatus = await _context.OrderStatus.FindAsync(id);
            if (orderStatus == null) return NotFound();
            var orderStatusResponse = _mapper.Map<OrderStatusResponse>(orderStatus);

            return Ok(orderStatusResponse);
        }

        /// <summary>
        /// Создать статус
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateOrderStatus(OrderStatusNew request)
        {
            var orderStatus = _mapper.Map<OrderStatus>(request);

            _context.OrderStatus.Add(orderStatus);
            await _context.SaveChangesAsync();

            return Ok(new { id = orderStatus.Id });
        }

        /// <summary>
        /// Изменить данные статуса
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> EditOrderStatus(OrderStatusEdit request)
        {
            var orderStatus = await _context.OrderStatus.FindAsync(request.Id);
            if (orderStatus == null) return NotFound();

            orderStatus.Title = request.Title;
           
            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Удалить статус по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteOrderStatus(Guid id)
        {
            var orderStatus = await _context.OrderStatus.FindAsync(id);
            if (orderStatus == null) return NotFound();
            _context.OrderStatus.Remove(orderStatus);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}