
using API.Models.Order;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.Domain;
using DataAccess;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace API.Controllers
{
    /// <summary>
    /// Продукты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public OrdersController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные по всем заказам
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Product>>> GetOrders()
        {
            var orders = await _context.Order
                .ProjectTo<OrderResponse>(_mapper.ConfigurationProvider).ToListAsync();

            return Ok(orders);
        }

        /// <summary>
        /// Получить данные заказа по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<OrderResponse>> GetOrder(Guid id)
        {
            var order = await _context.Order.FindAsync(id);
            if (order == null) return NotFound();
            var orderResponse = _mapper.Map<OrderResponse>(order);

            return Ok(orderResponse);
        }

        /// <summary>
        /// Создать заказ
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateOrder(OrderNew request)
        {
            var order = new Order()
            {
                Id = new Guid(),
                Number = request.Number,
                Phone = request.Phone,
                Address = request.Address,
                Description = request.Description,

                User = _context.User.FirstOrDefault(x => x.Id == request.UserId),
                Status = _context.OrderStatus.FirstOrDefault(x => x.Id == request.StatusId),
                Promocode = _context.Promocode.FirstOrDefault(x => x.Id == request.PromocodeId)
            };

            _context.Order.Add(order);
            await _context.SaveChangesAsync();

            return Ok(new { id = order.Id });
        }

        /// <summary>
        /// Изменить данные заказ
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> EditOrder(OrderEdit request)
        {
            var order = await _context.Order.FindAsync(request.Id);
            if (order == null) return NotFound();

            order.Number = request.Number;
            order.Phone = request.Phone;
            order.Address = request.Address;
            order.Description = request.Description;
            order.User = _context.User.FirstOrDefault(x => x.Id == request.UserId);
            order.Status = _context.OrderStatus.FirstOrDefault(x => x.Id == request.StatusId);
            order.Promocode = _context.Promocode.FirstOrDefault(x => x.Id == request.PromocodeId);

            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Удалить заказ по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteOrder(Guid id)
        {
            var order = await _context.Order.FindAsync(id);
            if (order == null) return NotFound();
            _context.Order.Remove(order);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}