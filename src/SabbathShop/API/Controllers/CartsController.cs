using API.Models.Cart;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.Domain;
using DataAccess;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace API.Controllers
{
    /// <summary>
    /// Продукты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CartsController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public CartsController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные по всем карточкам
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Cart>>> GetCarts()
        {
            var carts = await _context.Cart
                .ProjectTo<CartResponse>(_mapper.ConfigurationProvider).ToListAsync();

            return Ok(carts);
        }

        /// <summary>
        /// Получить данные карточки по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CartResponse>> GetCart(Guid id)
        {
            var cart = await _context.Cart.FindAsync(id);
            if (cart == null) return NotFound();
            var cartResponse = _mapper.Map<CartResponse>(cart);

            return Ok(cartResponse);
        }

        /// <summary>
        /// Создать карточку 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCart(CartNew request)
        {
            var cart = new Cart()
            {
                Id = new Guid(),
                Quantity = request.Quantity,
                User = _context.User.FirstOrDefault(x => x.Id == request.UserId),
                Product = _context.Product.FirstOrDefault(x => x.Id == request.ProductId),
            };

            _context.Cart.Add(cart);
            await _context.SaveChangesAsync();

            return Ok(new { id = cart.Id });
        }

        /// <summary>
        /// Изменить данные карточки
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> EditCart(CartEdit request)
        {
            var cart = await _context.Cart.FindAsync(request.Id);
            if (cart == null) return NotFound();

            cart.Quantity = request.Quantity;
            cart.Product = _context.Product.FirstOrDefault(x => x.Id == request.ProductId);
            cart.User = _context.User.FirstOrDefault(x => x.Id == request.UserId);
           
            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Удалить карточку по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCart(Guid id)
        {
            var cart = await _context.Cart.FindAsync(id);
            if (cart == null) return NotFound();
            _context.Cart.Remove(cart);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}