using API.Middlewares;
using API.Models.Category;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.Domain;
using DataAccess;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace API.Controllers
{
    /// <summary>
    /// Категории 
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public CategoryController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные по всем категориям
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(ResponseWrapperMiddleware.CommonApiResponse<IEnumerable<CategoryResponse>>))]
        public async Task<ActionResult<List<Category>>> GetCategorys()
        {
            var categorys = await _context.Category
                .ProjectTo<CategoryResponse>(_mapper.ConfigurationProvider).ToListAsync();

            return Ok(categorys);
        }

        /// <summary>
        /// Получить данные категории по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(200, Type = typeof(ResponseWrapperMiddleware.CommonApiResponse<CategoryResponse>))]
        public async Task<ActionResult<CategoryResponse>> GetCategory(Guid id)
        {
            var category = await _context.Category.FindAsync(id);
            if (category == null) return NotFound();
            var categoryResponse = _mapper.Map<CategoryResponse>(category);

            return Ok(categoryResponse);
        }

        /// <summary>
        /// Создать категорию
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(200, Type = typeof(ResponseWrapperMiddleware.CommonApiResponse<string>))]
        public async Task<IActionResult> CreateCategory(CategoryNew request)
        {
            var category = _mapper.Map<Category>(request);

            _context.Category.Add(category);
            await _context.SaveChangesAsync();

            return Ok(category.Id);
        }

        /// <summary>
        /// Изменить данные категории
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(200, Type = typeof(ResponseWrapperMiddleware.CommonApiResponse<bool>))]
        public async Task<IActionResult> EditCategory(CategoryEdit request)
        {
            var category = await _context.Category.FindAsync(request.Id);
            if (category == null) return NotFound();

            category.Title = request.Title;
           
            await _context.SaveChangesAsync();

            return Ok(true);
        }

        /// <summary>
        /// Удалить категорию по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(200, Type = typeof(ResponseWrapperMiddleware.CommonApiResponse<bool>))]
        public async Task<IActionResult> DeleteCategory(Guid id)
        {
            var role = await _context.Category.FindAsync(id);
            if (role == null) return NotFound();
            _context.Category.Remove(role);
            await _context.SaveChangesAsync();

            return Ok(true);
        }
    }
}