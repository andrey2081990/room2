using API.Models.User;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.Domain;
using DataAccess;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace API.Controllers
{
    /// <summary>
    /// Пользователи
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public UsersController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные по всем пользователям
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<User>>> GetUsers()
        {
            var users = await _context.User
                .ProjectTo<UserResponse>(_mapper.ConfigurationProvider).ToListAsync();

            return Ok(users);
        }

        /// <summary>
        /// Получить данные пользователя по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<UserResponse>> GetUser(Guid id)
        {
            var user = await _context.User.FindAsync(id);
            if (user == null) return NotFound();
            var userResponse = _mapper.Map<UserResponse>(user);

            return Ok(userResponse);
        }

        /// <summary>
        /// Создать пользователя
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateUser(UserNew request)
        {
            var user = _mapper.Map<User>(request);

            Role role = _context.Role.FirstOrDefault(x => x.Title == request.Role);
            if (role == null) return NotFound();

            user.RoleId = role.Id;
            user.Roles = new List<Role>();
            user.Roles.Add(role);
            _context.User.Add(user);
            await _context.SaveChangesAsync();

            return Ok(new { id = user.Id });
        }

        /// <summary>
        /// Изменить данные пользователя
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> EditUser(UserEdit request)
        {
            var user = await _context.User.FindAsync(request.Id);
            if (user == null) return NotFound();

            user.FirstName = request.FirstName;
            user.LastName = request.LastName;
            user.Email = request.Email;
            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Удалить пользователя по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteUser(Guid id)
        {
            var user = await _context.User.FindAsync(id);
            if (user == null) return NotFound();
            _context.User.Remove(user);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}