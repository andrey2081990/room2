
using API.Models.Promocode;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.Domain;
using DataAccess;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace API.Controllers
{
    /// <summary>
    /// Продукты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public PromocodesController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные по всем промокодам
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Promocode>>> GetPromocodes()
        {
            var promocodes = await _context.Promocode
                .ProjectTo<PromocodeResponse>(_mapper.ConfigurationProvider).ToListAsync();

            return Ok(promocodes);
        }

        /// <summary>
        /// Получить данные промокода по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PromocodeResponse>> GetPromocode(Guid id)
        {
            var promocode = await _context.Promocode.FindAsync(id);
            if (promocode == null) return NotFound();
            var promocodeResponse = _mapper.Map<PromocodeResponse>(promocode);

            return Ok(promocodeResponse);
        }

        /// <summary>
        /// Создать промокод
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreatePromocode(PromocodeNew request)
        {
            var promocode = _mapper.Map<Promocode>(request);

            _context.Promocode.Add(promocode);
            await _context.SaveChangesAsync();

            return Ok(new { id = promocode.Id });
        }

        /// <summary>
        /// Изменить данные промокода
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> EditPromocode(PromocodeEdit request)
        {
            var promocode = await _context.Promocode.FindAsync(request.Id);
            if (promocode == null) return NotFound();

            promocode.IsActive = request.IsActive;
            promocode.Title = request.Title;
            promocode.Nominal = request.Nominal;
            promocode.IsPercentage = request.IsPercentage;
           
            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Удалить промокод по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeletePromocode(Guid id)
        {
            var promocode = await _context.Promocode.FindAsync(id);
            if (promocode == null) return NotFound();
            _context.Promocode.Remove(promocode);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}