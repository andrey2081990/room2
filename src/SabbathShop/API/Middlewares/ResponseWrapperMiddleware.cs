using System.Net;
using Newtonsoft.Json;

namespace API.Middlewares;

public class ResponseWrapperMiddleware
{
    private readonly RequestDelegate _next;

    public ResponseWrapperMiddleware(RequestDelegate next)
    {
        this._next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        var currentBody = context.Response.Body;

        using (var memoryStream = new MemoryStream())
        {
            //set the current response to the memorystream.
            context.Response.Body = memoryStream;
            await _next(context);
            //reset the body 
            context.Response.Body = currentBody;
            memoryStream.Seek(0, SeekOrigin.Begin);

            var readToEnd = new StreamReader(memoryStream).ReadToEnd();
            object objResult = JsonConvert.DeserializeObject(readToEnd);
            var result = CommonApiResponse<object>.Create((HttpStatusCode)context.Response.StatusCode, objResult);
            await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
        }
    }

    public class CommonApiResponse<T>
    {
        public static CommonApiResponse<T> Create(HttpStatusCode statusCode, T result, string errorMessage = null)
        {
            return new CommonApiResponse<T>(statusCode, result, errorMessage);
        }

        public int StatusCode { get; set; }
        public string RequestId { get; }
        public string ErrorMessage { get; set; }
        public T Result { get; set; }

        protected CommonApiResponse(HttpStatusCode statusCode, T result, string errorMessage = null)
        {
            RequestId = Guid.NewGuid().ToString();
            StatusCode = (int)statusCode;
            Result = result;
            ErrorMessage = errorMessage;
        }
    }
}