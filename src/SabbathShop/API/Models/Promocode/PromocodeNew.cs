﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Promocode
{
    public class PromocodeNew
    {
        public bool IsActive { get; set; }
        public string Title { get; set; }
        public decimal Nominal { get; set; }
        public bool IsPercentage { get; set; }
    }
}
