﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Category
{
    public class CategoryResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}
