﻿using API.Models.OrderStatus;
using API.Models.Promocode;
using API.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Order
{
    public class OrderResponse
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public string Phone { get; set; }
        public int Address { get; set; }
        public int Description { get; set; }
        public UserResponse User { get; set; }
        public OrderStatusResponse Status { get; set; }
        public PromocodeResponse  Promocode { get; set; }
    }
}
