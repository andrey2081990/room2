﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Sale
{
    public class SaleNew
    {
        public string Quantity { get; set; }
        public int Percentage { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public Guid ProductId { get; set; }
    }
}
