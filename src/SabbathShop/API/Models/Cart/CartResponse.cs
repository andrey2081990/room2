﻿using API.Models.Product;
using API.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Cart
{
    public class CartResponse
    {
        public Guid Id { get; set; }
        public string Quantity { get; set; }
        public UserResponse User { get; set; }
        public ProductResponse Product{ get; set; }
    }
}
