﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Cart
{
    public class CartNew
    {
        public string Quantity { get; set; }
        public Guid UserId { get; set; }
        public Guid ProductId { get; set; }
    }
}
