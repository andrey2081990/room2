﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Product
{
    public class ProductEdit
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public int Sort { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
