﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Product
{
    public class ProductResponse
    {
        public Guid Id { get; set; }
        public int Sort { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string MediaUrl { get; set; }
        public string Category { get; set; }
    }
}
