﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.OrderStatus
{
    public class OrderStatusEdit
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}
