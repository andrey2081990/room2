﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Role
{
    public class RoleEdit
    {
        public Guid Id { get; set; }
        public int Sort { get; set; }
        public string Title { get; set; }
       
    }
}
