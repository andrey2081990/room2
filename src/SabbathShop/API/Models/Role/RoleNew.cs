﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Role
{
    public class RoleNew
    {
        public int Sort { get; set; }
        public string Title { get; set; }
    }
}
