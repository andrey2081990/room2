import {memo} from 'react';
import {CategoryList} from "../components";
export const WelcomePage = memo((): JSX.Element => {
    return (
        <div>
            Я главная страница и тут список категорий
            <CategoryList />
        </div>
    );
});
