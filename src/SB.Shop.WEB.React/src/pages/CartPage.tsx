import {memo} from 'react';
import {Link} from "react-router-dom";
export const CartPage = memo((): JSX.Element => {
    return (
        <div>
            Я страница-корзина с минимальным дизайном
            <Link to={'/'}>На главную</Link>
        </div>
    );
});
