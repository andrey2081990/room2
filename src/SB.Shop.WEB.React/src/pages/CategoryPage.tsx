import {memo} from 'react';
import {CategoryInfo} from "../components/CategoryInfo/CategoryInfo.tsx";

export const CategoryPage = memo((): JSX.Element => {
    return (
        <div>
            Я страница конкретной категории
            <CategoryInfo />
        </div>
    );
});
