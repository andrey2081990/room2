import {ReactNode} from 'react';

export interface UiLayoutProps {
    children?: ReactNode;
}
