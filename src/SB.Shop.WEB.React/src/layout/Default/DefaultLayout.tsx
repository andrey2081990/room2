import {memo} from 'react';
import {useLocation} from 'react-router-dom';

import {UiLayoutProps} from '../types';

import './DefaultLayout.scss';
import {Navigation} from "@/components";
export const DefaultLayout = memo(({children}: UiLayoutProps): JSX.Element => {
    const location = useLocation();

    return (
        <div className={'page page_' + location.pathname.toLowerCase().replace('/', '')}>
            <div className="default-layout">
                <header><Navigation /></header>
                <main className="default-layout__main">{children}</main>
                <footer>Я подвал</footer>
            </div>
        </div>
    );
});
