import {memo} from 'react';
import {useLocation} from 'react-router-dom';

import {UiLayoutProps} from '../types';

import './ClearLayout.scss';

export const ClearLayout = memo(({children}: UiLayoutProps): JSX.Element => {
    const location = useLocation();

    return (
        <div className={'page page_' + location.pathname.toLowerCase().replace('/', '')}>
            <div className="clear-layout">
                <main className="default-layout__main">{children}</main>
            </div>
        </div>
    );
});
