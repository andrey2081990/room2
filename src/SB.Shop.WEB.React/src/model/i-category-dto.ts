export type ICategoryDto = {
    id: number;
    title: string;
}