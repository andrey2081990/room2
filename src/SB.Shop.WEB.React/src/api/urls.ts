const BASE_PATH = `/api/v1`;
export const urls = {
    shop: {
        category: `${BASE_PATH}/category`,
        categoryById: (id: string) => `${BASE_PATH}/category/${id}`
    }
}