import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from 'axios';

import {appConfig} from '../config/app.config';
import {
    ICategoryDto
} from '../model';

import type {ApiBadRequestError, ApiResponse} from './types';
import {urls} from "./urls.ts";

export const API_URL = new URL(appConfig.apiUrl);

const ax = axios.create({baseURL: API_URL.href, withCredentials: false});

//TODO: remove ignore
// @ts-ignore
ax.interceptors.request.use(requestHandler);
ax.interceptors.response.use(responseHandler, responseErrorHandler);

export async function getCategory(): Promise<Array<ICategoryDto>> {
    const response = await ax.get<ApiResponse<Array<ICategoryDto>>>(urls.shop.category);

    return response.data.Result;
}

export async function getCategoryById(id: string): Promise<ICategoryDto> {
    const response = await ax.get<ApiResponse<ICategoryDto>>(urls.shop.categoryById(id));

    return response.data.Result;
}


function requestHandler(request: AxiosRequestConfig): AxiosRequestConfig {
    // eslint-disable-next-line no-console
    console.log('[API req]', request);

    return request;
}

function responseHandler<T>(response: AxiosResponse<T>): AxiosResponse<T> {
    // eslint-disable-next-line no-console
    console.log('[API resp]', response);

    return response;
}

function responseErrorHandler(responseError: AxiosError<unknown>): Promise<ApiBadRequestError> {
    // eslint-disable-next-line no-console
    console.error('[API error]', responseError.response);

    return Promise.reject(responseError.response?.statusText);
}
