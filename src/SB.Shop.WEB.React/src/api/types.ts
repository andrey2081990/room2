/* eslint-disable camelcase */

export interface ApiResponse<T> {
    StatusCode: number;
    RequestId: string;
    ErrorMessage: string | null;
    Result: T;
}

export interface ApiBadRequestError {
    reason: string;
    body?: {
        description: string;
    };
    status?: number;
}
