import {createBrowserRouter} from "react-router-dom";
import {DefaultLayout, ClearLayout} from "./layout";
import {CartPage, CategoryPage, ProductPage, WelcomePage} from "./pages";

export  const routes = createBrowserRouter([
    {
        path: "/",
        element: <DefaultLayout><WelcomePage /></DefaultLayout>,
    },
    {
        path: "/category/:id",
        element: <DefaultLayout><CategoryPage /></DefaultLayout>,
    },
    {
        path: "/item",
        element: <DefaultLayout><ProductPage /></DefaultLayout>,
    },
    {
        path: "/cart",
        element: <ClearLayout><CartPage /></ClearLayout>,
    },
]);