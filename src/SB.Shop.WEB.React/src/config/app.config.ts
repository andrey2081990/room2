import {ConfigModeType} from './type';

const isProd = process.env.NODE_ENV === 'production';
export const appConfig = {
    mode: isProd ? ConfigModeType.PRODUCTION : ConfigModeType.DEVELOPMENT,
    apiUrl: isProd ? 'https://api.super-puper-shop.ru' : 'http://localhost:5001',
    localStorageItemName: 'sabbath-shop'
};
