import {memo} from 'react';
import {Link} from "react-router-dom";

import './Navigation.scss';

export const Navigation = memo((): JSX.Element => {

    return (
        <>
            <nav className={'navigation'}>
                <Link to={'/'} >На главную</Link>
                <Link to={'/cart'} >Корзина</Link>
            </nav>
        </>
    );
});
