import {memo, useEffect, useState} from 'react';

import './CategoryList.scss';
import {ICategoryDto} from "../../model";
import {CategoryListItem} from "./Item.tsx";
import {getCategory} from "../../api";

export const CategoryList = memo((): JSX.Element => {
    const [items, setItems] = useState<Array<ICategoryDto>>([]);

    useEffect(() => {
        getCategory().then(r => setItems(r));
    }, [])

    return (
        <>
            <div className={'category-list'}>
                {items.map((c:ICategoryDto) => <CategoryListItem key={c.id} item={c} />)}
            </div>
        </>
    );
});
