import {memo} from 'react';
import {Link} from "react-router-dom";
import {ICategoryDto} from "../../model";

export const CategoryListItem = memo(({item}: CategoryListItemProps): JSX.Element => <Link to={`/category/${item.id}`}>{item.title}</Link>);

type CategoryListItemProps = {
    item: ICategoryDto
}