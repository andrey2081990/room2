import {memo, useEffect, useState} from 'react';

import './CategoryInfo.scss';
import {ICategoryDto} from "../../model";
import {getCategoryById} from "../../api";
import {useParams} from "react-router-dom";

export const CategoryInfo = memo((): JSX.Element => {
    const params = useParams();


    const [data, setData] = useState<ICategoryDto>();

    useEffect(() => {
        if (params.id) {
            getCategoryById(params.id).then(r => setData(r));
        }
    }, [])

    return (
        <>
            {data && (
                <div className={'category-info'}>
                    <h1> {data.title}</h1>
                </div>)}
        </>
    );
});